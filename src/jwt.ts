import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import { jwtCongig } from "./config";

export const generateAccessToken = (data: any) => {
  console.log("data generate: ", data);
  return jwt.sign({ data }, jwtCongig?.jwtSecret);
};

export const generateRefreshToken = (data: any) => {
  return jwt.sign({ data }, "myscret210", { expiresIn: 100000 });
};

export const authenToken = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const authorizationHeader = req.headers["authorization"];
  const token = authorizationHeader?.split(" ")[1];
  if (!token) {
    res.sendStatus(401);
  } else {
    jwt.verify(token, jwtCongig?.jwtSecret, (err, resp: any) => {
      delete resp?.data?.password;
      res.json({ data: resp?.data });
      if (err) resp?.data.sendStatus(403);
      next();
    });
  }
};

export const refreshToken = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // 'Beaer [token]'
  const { refreshToken } = req?.body;
  if (!refreshToken) {
    res.sendStatus(401);
  } else {
    jwt.verify(refreshToken, "myscret210", (err: any, resp: any) => {
      const accessToken = generateAccessToken(resp?.data);
      res.json({
        accessToken,
        refreshToken,
      });
      if (err) resp?.data.sendStatus(403);
    });
  }
};
