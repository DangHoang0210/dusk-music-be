import { songRepository } from "../repository/song.repository";
import { Request, Response } from "express";
import { join } from "path";
import { getAudioDurationInSeconds } from "get-audio-duration";
import { jwtCongig } from "../config";
import { userRepository } from "../repository";
import jwt from "jsonwebtoken";
import { categoryRepository } from "../repository/category.repository";
import { songReportRepository } from "../repository/song-report.repository";
import { playlistRepository } from "../repository/playlist.repository";
import { artistRepository } from "../repository/artist.repository";
import { albumRepository } from "../repository/album.repository";
import { playlistReportRepository } from "../repository/playlist-report.repository";
import { albumReportRepository } from "../repository/album-report.repository";
import { songStatistialRepository } from "../repository/song-statistical.repository";

export const getNewReleaseSongs = async (req: Request, res: Response) => {
  const { limit, offset } = req.query;
  const total = await songRepository.count();

  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        const songs = await songRepository.find({
          order: {
            createdAt: "DESC",
          },
          where: {
            status: 0,
            is_deleted: 0,
          },
          take: Number(limit),
          skip: Number(offset),
          relations: ["artists"],
        });

        if (user) {
          const songIds = songs?.map((item) => item?.id);
          const listenState = await getSongReport(songIds);
          const songState = await getLikeState(songIds);

          const data = songs.map((song) => {
            const songReport = listenState?.find(
              (item) => item?.id === song?.id
            );
            const likedState = songState?.find((item) => item?.id === song?.id);
            return {
              ...song,
              artists: song.artists.map((artist) => ({
                id: artist.id,
                name: artist.name,
              })),
              likeCount: songReport?.likeCount,
              listenCount: songReport?.listenTotal,
              isLiked: likedState?.isLiked,
            };
          });

          res.json({
            items: data,
            total: total,
          });
        } else {
          const data = songs.map((song) => {
            return {
              ...song,
              artists: song.artists.map((artist) => ({
                id: artist.id,
                name: artist.name,
              })),
              isLiked: false,
            };
          });
          res.json({
            items: data,
            total: total,
          });
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getSongs = async (req: Request, res: Response) => {
  const { limit, offset, userId } = req.query;

  const total = await songRepository
    .createQueryBuilder("song")
    .where("song.is_deleted = 0")
    .getCount();

  try {
    const songs = await songRepository.find({
      order: {
        createdAt: "DESC",
      },
      where: {
        status: 0,
        is_deleted: 0,
      },
      take: Number(limit),
      skip: Number(offset),
      relations: ["artists", "categories", "albums"],
    });

    const songIds = songs?.map((song) => song?.id);

    const rs = await getSongReport(songIds);
    const rs2 = await getLikeState(songIds);

    const data = songs.map((song) => {
      const songReport = rs?.find((item) => item?.id === song?.id);
      const likedState = rs2?.find((item) => item?.id === song?.id);
      return {
        ...song,
        artists: song.artists.map((artist) => ({
          id: artist.id,
          name: artist.name,
        })),
        likeCount: songReport?.likeCount,
        listenCount: songReport?.listenTotal,
        isLiked: likedState?.isLiked,
      };
    });

    res.json({
      items: data,
      total: total,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getCurrentSong = async (req: Request, res: Response) => {
  const { songId } = req.query;

  try {
    const song = await songRepository.findOne({
      where: {
        is_deleted: 0,
        id: Number(songId),
      },
      relations: ["artists", "categories", "albums"],
    });

    const rs = await getSongReport([Number(songId)]);
    const rs2 = await getLikeState([Number(songId)]);

    const songReport = rs?.find((item) => item?.id === Number(songId));
    const likedState = rs2?.find((item) => item?.id === Number(songId));

    res.json({
      ...song,
      likeCount: songReport?.likeCount,
      listenCount: songReport?.listenTotal,
      isLiked: likedState?.isLiked,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getStatisticalByDay = async (req: Request, res: Response) => {
  const { day } = req.query;

  try {
    if (day) {
      const selectedDay = new Date(day?.toString());
      const currentMonth = selectedDay.getMonth() + 1;
      const currentYear = selectedDay.getFullYear();
      const currentDate = selectedDay.getDate();
      const songStatiscial = await songRepository
        .createQueryBuilder("song")
        .where("song.is_deleted = 0")
        .andWhere(`EXTRACT(MONTH FROM song.createdAt) = :currentMonth`, {
          currentMonth,
        })
        .andWhere(`EXTRACT(YEAR FROM song.createdAt) = :currentYear`, {
          currentYear,
        })
        .andWhere(`EXTRACT(DAY FROM song.createdAt) = :currentDate`, {
          currentDate,
        })
        .getCount();
      const userStatiscial = await userRepository
        .createQueryBuilder("user")
        .where("user.is_deleted = 0")
        .andWhere(`EXTRACT(MONTH FROM user.createdAt) = :currentMonth`, {
          currentMonth,
        })
        .andWhere(`EXTRACT(YEAR FROM user.createdAt) = :currentYear`, {
          currentYear,
        })
        .andWhere(`EXTRACT(DAY FROM user.createdAt) = :currentDate`, {
          currentDate,
        })
        .getCount();

      const artistStatistical = await artistRepository
        .createQueryBuilder("artist")
        .where("artist.is_deleted = 0")
        .andWhere(`EXTRACT(MONTH FROM artist.createdAt) = :currentMonth`, {
          currentMonth,
        })
        .andWhere(`EXTRACT(YEAR FROM artist.createdAt) = :currentYear`, {
          currentYear,
        })
        .andWhere(`EXTRACT(DAY FROM artist.createdAt) = :currentDate`, {
          currentDate,
        })
        .getCount();

      const albumStatistical = await albumRepository
        .createQueryBuilder("album")
        .where("album.is_deleted = 0")
        .andWhere(`EXTRACT(MONTH FROM album.createdAt) = :currentMonth`, {
          currentMonth,
        })
        .andWhere(`EXTRACT(YEAR FROM album.createdAt) = :currentYear`, {
          currentYear,
        })
        .andWhere(`EXTRACT(DAY FROM album.createdAt) = :currentDate`, {
          currentDate,
        })
        .getCount();

      const playlistStatistical = await playlistRepository
        .createQueryBuilder("playlist")
        .where("playlist.is_deleted = 0")
        .andWhere(`EXTRACT(MONTH FROM playlist.createdAt) = :currentMonth`, {
          currentMonth,
        })
        .andWhere(`EXTRACT(YEAR FROM playlist.createdAt) = :currentYear`, {
          currentYear,
        })
        .andWhere(`EXTRACT(DAY FROM playlist.createdAt) = :currentDate`, {
          currentDate,
        })
        .getCount();
      const categoryStatistical = await categoryRepository
        .createQueryBuilder("category")
        .where("category.is_deleted = 0")
        .andWhere(`EXTRACT(MONTH FROM category.createdAt) = :currentMonth`, {
          currentMonth,
        })
        .andWhere(`EXTRACT(YEAR FROM category.createdAt) = :currentYear`, {
          currentYear,
        })
        .andWhere(`EXTRACT(DAY FROM category.createdAt) = :currentDate`, {
          currentDate,
        })
        .getCount();

      res.json([
        {
          name: "Songs",
          value: songStatiscial,
        },
        {
          name: "Users",
          value: userStatiscial,
        },
        {
          name: "Artists",
          value: artistStatistical,
        },
        {
          name: "Albums",
          value: albumStatistical,
        },
        {
          name: "Playlists",
          value: playlistStatistical,
        },
        // {
        //   name: "Category",
        //   value: categoryStatistical,
        // },
      ]);
    } else {
      res.status(412).send("Unselected time");
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const updateSong = async (req: Request, res: Response) => {
  const { id, songThumb, songName, songArtists, songCategories } = req.body;
  try {
    const currentSong = await songRepository.findOne({
      where: {
        id,
        is_deleted: 0,
        status: 0,
      },
    });

    if (currentSong) {
      currentSong.name = songName;
      currentSong.imageUrl = songThumb;
      currentSong.artists = songArtists;
      currentSong.categories = songCategories;
      await songRepository.save(currentSong);
      res.json("Update successfully !");
    } else {
      res.json("Song not found!");
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const updateUploadedSong = async (req: Request, res: Response) => {
  const { id, songThumb, songName } = req.body;
  try {
    const currentSong = await songRepository.findOne({
      where: {
        id,
        is_deleted: 0,
        status: 1,
      },
    });

    if (currentSong) {
      currentSong.name = songName;
      currentSong.imageUrl = songThumb;
      await songRepository.save(currentSong);
      res.json("Update successfully !");
    } else {
      res.json("Song not found!");
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getSongReport = async (songIds: number[]) => {
  if (songIds?.length === 0) return [];
  const songReports = await songReportRepository
    .createQueryBuilder("songReport")
    .where("songReport.song_id IN (:...songIds)", { songIds })
    .andWhere("songReport.is_deleted = 0")
    .getMany();

  const songRs = songIds?.map((id) => {
    const likeCount = songReports?.filter(
      (item) => item?.song_id === id && item?.type === 2
    );
    const listenCount = songReports?.filter(
      (item) => item?.song_id === id && item?.type === 1
    );

    const listenTotal = listenCount?.reduce((sum, arg) => sum + arg?.count, 0);

    return {
      id,
      likeCount: likeCount?.length,
      listenTotal,
    };
  });
  return songRs;
};

export const getLikeState = async (songIds: number[]) => {
  const songReports = await songReportRepository
    .createQueryBuilder("songReport")
    .where("songReport.song_id IN (:...songIds)", { songIds })
    .andWhere("songReport.is_deleted = 0")
    .andWhere("songReport.type = 2")
    .getMany();

  const rs = songIds?.map((id) => {
    const likedState = songReports?.find((song) => song?.song_id === id);
    return {
      id,
      isLiked: likedState ? true : false,
    };
  });

  return rs;
};

export const searchByKeyword = async (req: Request, res: Response) => {
  const { keyword } = req.params;

  const songResult = await songRepository
    .createQueryBuilder("song")
    .leftJoinAndSelect("song.artists", "artists")
    .where("LOWER(song.name) LIKE LOWER(:name)", { name: `%${keyword}%` })
    .andWhere("song.is_deleted = 0")
    .getMany();

  const songIds = songResult?.map((song) => song?.id);

  const listenState = await getSongReport(songIds);
  const songState = await getLikeState(songIds);

  const songs = songResult?.map((song) => {
    const songReport = listenState?.find((item) => item?.id === song?.id);
    const likedState = songState?.find((item) => item?.id === song?.id);
    return {
      ...song,
      artists: song?.artists?.map((artist) => ({
        id: artist.id,
        name: artist.name,
      })),
      likeCount: songReport?.likeCount,
      listenCount: songReport?.listenTotal,
      isLiked: likedState?.isLiked,
    };
  });

  const playlists = await playlistRepository
    .createQueryBuilder("playlist")
    .leftJoinAndSelect("playlist.songs", "songs")
    .where("LOWER(playlist.name) LIKE LOWER(:name)", { name: `%${keyword}%` })
    .andWhere("playlist.is_deleted = 0")
    // .andWhere("playlist.user_id = :id", {id: user?.id})
    .getMany();

  const albums = await albumRepository
    .createQueryBuilder("album")
    .where("LOWER(album.name) LIKE LOWER(:name)", { name: `%${keyword}%` })
    .andWhere("album.is_deleted = 0")
    .getMany();

  const artists = await artistRepository
    .createQueryBuilder("artist")
    .where("LOWER(artist.name) LIKE LOWER(:name)", { name: `%${keyword}%` })
    .andWhere("artist.is_deleted = 0")
    .getMany();

  res.json({
    songs,
    playlists,
    albums,
    artists,
  });
};

export const getRecentListen = async (req: Request, res: Response) => {
  const { limit, offset } = req.query;

  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          // GET song
          const songReports = await songReportRepository
            .createQueryBuilder("songReport")
            .where("songReport.user_id = :id", { id: user?.id })
            .andWhere("songReport.is_deleted = 0")
            .andWhere("songReport.type = 1")
            .orderBy("songReport.updatedAt", "DESC")
            .take(Number(limit))
            .skip(Number(offset))
            .getMany();

          const songIds = songReports?.map((song) => song?.song_id);

          const songs = await songRepository
            .createQueryBuilder("song")
            .leftJoinAndSelect("song.artists", "artists")
            .where("song.id IN (:...songIds)", { songIds })
            .andWhere("song.is_deleted = 0")
            .getMany();

          const rs = await getSongReport(songIds);
          const rs2 = await getLikeState(songIds);

          const data = songs.map((song) => {
            const songReport = rs?.find((item) => item?.id === song?.id);
            const likedState = rs2?.find((item) => item?.id === song?.id);
            return {
              ...song,
              likeCount: songReport?.likeCount,
              listenCount: songReport?.listenTotal,
              isLiked: likedState?.isLiked,
            };
          });

          // GET playlist
          const playlistReports = await playlistReportRepository
            .createQueryBuilder("playlistReport")
            .where("playlistReport.user_id = :id", { id: user?.id })
            .andWhere("playlistReport.is_deleted = 0")
            .andWhere("playlistReport.type = 1")
            .orderBy("playlistReport.updatedAt", "DESC")
            .take(Number(limit))
            .skip(Number(offset))
            .getMany();

          const playlistIds = playlistReports?.map((song) => song?.playlist_id);

          const playlists = await playlistRepository
            .createQueryBuilder("playlist")
            .leftJoinAndSelect("playlist.songs", "songs")
            .where("playlist.id IN (:...playlistIds)", { playlistIds })
            .andWhere("playlist.is_deleted = 0")
            .getMany();

          // GET album
          const albumReports = await albumReportRepository
            .createQueryBuilder("albumReport")
            .where("albumReport.user_id = :id", { id: user?.id })
            .andWhere("albumReport.is_deleted = 0")
            .andWhere("albumReport.type = 1")
            .orderBy("albumReport.updatedAt", "DESC")
            .take(Number(limit))
            .skip(Number(offset))
            .getMany();

          const albumIds = albumReports?.map((song) => song?.album_id);

          const albums = await albumRepository
            .createQueryBuilder("album")
            .leftJoinAndSelect("album.songs", "songs")
            .where("album.id IN (:...albumIds)", { albumIds })
            .andWhere("album.is_deleted = 0")
            .getMany();

          const albumResponse = albums?.map((album) => {
            const imageUrl = album?.imageUrl
              ? album?.imageUrl
              : album?.songs?.length > 0
              ? album?.songs?.[0]?.imageUrl
              : null;
            return {
              ...album,
              imageUrl,
            };
          });

          res.json({
            songs: data,
            playlists,
            albums: albumResponse,
          });
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const getFavouriteSongs = async (req: Request, res: Response) => {
  const { limit, offset } = req.query;

  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const songReports = await songReportRepository
            .createQueryBuilder("songReport")

            .where("songReport.user_id = :id", { id: user?.id })
            .andWhere("songReport.is_deleted = 0")
            .andWhere("songReport.type = 2")
            .take(Number(limit))
            .skip(Number(offset))
            .getMany();

          const songTotal = await songReportRepository
            .createQueryBuilder("songReport")
            .where("songReport.user_id = :id", { id: user?.id })
            .andWhere("songReport.is_deleted = 0")
            .andWhere("songReport.type = 2")
            .getCount();
          const favouriteSongIds = songReports.map((item) => item?.song_id);

          const data = await songRepository
            .createQueryBuilder("song")
            .leftJoinAndSelect("song.artists", "artists")
            .leftJoinAndSelect("song.albums", "albums")
            .orderBy("song.updatedAt", "DESC")
            .where("song.id IN (:...songIds)", { songIds: favouriteSongIds })
            .andWhere("song.is_deleted = 0")
            .getMany();

          const rs = data.map((song) => {
            return {
              ...song,
              artists: song.artists.map((artist) => ({
                id: artist.id,
                name: artist.name,
              })),
              isLiked: true,
            };
          });

          res.json({
            items: rs,
            total: songTotal,
          });
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const deleteSong = async (req: Request, res: Response) => {
  const { songId } = req.body;

  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
          relations: ["songs"],
        });

        if (user) {
          const song = await songRepository
            .createQueryBuilder("song")
            .where("song.id = :id", { id: songId })
            .andWhere("song.is_deleted = 0")
            .getOne();

          if (!song) {
            res.json({ status: 404, message: "Song not found!" });
          } else {
            if (user.is_admin || song?.created_by == user.id) {
              const songReports = await songReportRepository
                .createQueryBuilder("songReport")
                .where("songReport.song_id = :song_id", { song_id: songId })
                .andWhere("songReport.is_deleted = 0")
                .getMany();

              const rs = songReports.map((item) => {
                return {
                  ...item,
                  is_deleted: 1,
                };
              });

              song.is_deleted = 1;

              await songReportRepository.save(rs);
              await songRepository.save(song);

              // return;
              res.json({ status: 200, message: "Delete successfully!" });
            }
          }
        }

        // res.json({ status: 200, message: "Delete successfully!" });
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const deleteUserSong = async (req: Request, res: Response) => {
  const { songId } = req.body;

  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
          relations: ["songs"],
        });

        if (user) {
          const song = await songRepository
            .createQueryBuilder("song")
            .where("song.id = :id", { id: songId })
            .andWhere("song.status = 1")
            .andWhere("song.is_deleted = 0")
            .getOne();

          if (!song) {
            res.json({ status: 404, message: "Song not found!" });
          } else {
            if (user.is_admin || song?.created_by == user.id) {
              const songReports = await songReportRepository
                .createQueryBuilder("songReport")
                .where("songReport.song_id = :song_id", { song_id: songId })
                .andWhere("songReport.is_deleted = 0")
                .getMany();

              console.log("songReports: ", songReports);

              const rs = songReports.map((item) => {
                return {
                  ...item,
                  is_deleted: 1,
                };
              });

              song.is_deleted = 1;

              await songReportRepository.save(rs);
              await songRepository.save(song);

              // return;
              res.json({ status: 200, message: "Delete successfully!" });
            }
          }
        }

        // res.json({ status: 200, message: "Delete successfully!" });
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const getSongsByCategories = async (req: Request, res: Response) => {
  const { limit, offset, type } = req.query;
  try {
    const query = await categoryRepository
      .createQueryBuilder("category")
      .leftJoinAndSelect("category.songs", "songs")
      .where("category.is_deleted = 0")
      .andWhere("category.id = :id", { id: type })
      .orderBy("songs.createdAt", "DESC")
      .skip(Number(offset))
      .limit(Number(limit))
      .getOne();

    const totalCount = await categoryRepository
      .createQueryBuilder("category")
      .leftJoinAndSelect("category.songs", "songs")
      .where("category.is_deleted = 0")
      .andWhere("category.id = :id", { id: type })
      .select("COUNT(songs.id)", "total")
      .getRawOne();

    if (query) {
      const songIds = query?.songs?.map((item) => item?.id);
      const listenState = await getSongReport(songIds);
      const songState = await getLikeState(songIds);
      const songs = query?.songs?.map((song) => {
        const songReport = listenState?.find((item) => item?.id === song?.id);
        const likedState = songState?.find((item) => item?.id === song?.id);
        return {
          ...song,
          artists: song?.artists?.map((artist) => ({
            id: artist.id,
            name: artist.name,
          })),
          likeCount: songReport?.likeCount,
          listenCount: songReport?.listenTotal,
          isLiked: likedState?.isLiked,
        };
      });
      res.json({
        data: {
          ...query,
          songs,
        },
        total: Number(totalCount?.total),
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getSongsFromPlaylist = async (req: Request, res: Response) => {
  const { playlistId } = req.params;
  try {
    const data = await playlistRepository
      .createQueryBuilder("playlist")
      .leftJoinAndSelect("playlist.songs", "songs")
      .where("playlist.is_deleted = 0")
      .andWhere("playlist.id = :id", { id: playlistId })
      .getOne();

    if (!!data?.songs && data?.songs?.length > 0) {
      const songIds = data?.songs?.map((item) => item?.id);
      const listenState = await getSongReport(songIds);
      const songState = await getLikeState(songIds);

      const songFromPlaylist = data?.songs?.map((song) => {
        const songReport = listenState?.find((item) => item?.id === song?.id);
        const likedState = songState?.find((item) => item?.id === song?.id);
        return {
          ...song,
          artists: song?.artists?.map((artist) => ({
            id: artist.id,
            name: artist.name,
          })),
          likeCount: songReport?.likeCount,
          listenCount: songReport?.listenTotal,
          isLiked: likedState?.isLiked,
        };
      });
      res.json({
        data: {
          ...data,
          songs: songFromPlaylist,
        },
        total: data?.songs.length,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getUserUploadedSongs = async (req: Request, res: Response) => {
  const { limit, offset } = req.query;

  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
          relations: ["songs"],
        });

        if (user) {
          const total = await songRepository
            .createQueryBuilder("song")
            .where("song.is_deleted = 0")
            .andWhere("song.status = 1")
            .getCount();

          const songs = await songRepository.find({
            order: {
              createdAt: "DESC",
            },
            where: {
              status: 1,
              is_deleted: 0,
            },
            take: Number(limit),
            skip: Number(offset),
            relations: ["artists", "categories", "albums"],
          });

          const songIds = songs?.map((song) => song?.id);

          const rs = await getSongReport(songIds);
          const rs2 = await getLikeState(songIds);

          const data = songs.map((song) => {
            const songReport = rs?.find((item) => item?.id === song?.id);
            const likedState = rs2?.find((item) => item?.id === song?.id);
            return {
              ...song,
              likeCount: songReport?.likeCount,
              listenCount: songReport?.listenTotal,
              isLiked: likedState?.isLiked,
            };
          });

          res.json({
            songs: data,
            total: total,
          });
        }

        // res.json({ status: 200, message: "Delete successfully!" });
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const createNewUserSong = async (req: Request, res: Response) => {
  try {
    const { songThumb, songAudio, songDuration, songName } = req.body;
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
          relations: ["songs"],
        });

        if (user) {
          const newSong = await songRepository.create({
            name: songName,
            created_by: user.id,
            link: songAudio,
            duration: songDuration,
            imageUrl: songThumb,
            status: 1,
          });
          await songRepository.save(newSong);

          res.status(201).json(newSong);
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const uploadSongAudioAdmin = async (req: Request, res: Response) => {
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
          relations: ["songs"],
        });

        if (user) {
          if (!req.file) {
            res.status(400).json({ error: "No file uploaded" });
            return;
          }

          const { path, originalname } = req?.file;

          const duration = await getAudioDurationInSeconds(path);
          console.log("split: ", path?.split("uploads/"));

          res.status(201).json({
            link: `http://localhost:8180/api/audio/${
              path?.split("uploads/audios/")?.[1]
            }`,
            duration: duration,
          });
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const uploadSongThumbAdmin = async (req: Request, res: Response) => {
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
          relations: ["songs"],
        });

        if (user) {
          if (!req.file) {
            res.status(400).json({ error: "No file uploaded" });
            return;
          }

          const { path, originalname } = req?.file;
          res.status(201).json({
            link: `http://localhost:8180/api/image/${
              path?.split("uploads/images/")?.[1]
            }`,
          });
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const getAudio = async (req: Request, res: Response) => {
  const audioId = req.params?.audioId;
  res.sendFile(join(process.cwd(), "uploads/audios/" + audioId));
  // res.json(`http:localhost:8180/api/uploads/${audioId}`);
  try {
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const getImages = async (req: Request, res: Response) => {
  const imageId = req.params?.imageId;
  res.sendFile(join(process.cwd(), "uploads/images/" + imageId));
  // res.json(`http:localhost:8180/api/uploads/${audioId}`);
  try {
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const createNewSong = async (req: Request, res: Response) => {
  try {
    const {
      songThumb,
      songAudio,
      songDuration,
      songArtists,
      songName,
      songCategories,
    } = req.body;
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
          relations: ["songs"],
        });

        if (user) {
          const newSong = await songRepository.create({
            name: songName,
            created_by: user.id,
            link: songAudio,
            duration: songDuration,
            artists: songArtists,
            imageUrl: songThumb,
            categories: songCategories,
          });
          await songRepository.save(newSong);

          res.status(201).json(newSong);
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const getTrendingSongs = async (req: Request, res: Response) => {
  const { songLimitInput } = req.query;
  const songLimit = Number(songLimitInput);
  try {
    const trendingSongs = await songStatistialRepository
      .createQueryBuilder("songStatistical") // Specify the alias for the entity
      .select("songStatistical.song_id", "songId")
      .addSelect("SUM(songStatistical.count)", "listencount")
      .where("songStatistical.date >= NOW() - interval '7 day'")
      .groupBy("songStatistical.song_id")
      .orderBy("listencount", "DESC") // Use the alias "totalCount" directly in orderBy
      .limit(songLimit)
      .getRawMany();

    const songIds = trendingSongs?.map((song) => song?.songId);

    if (songIds?.length > 0) {
      const data = await songReportRepository
        .createQueryBuilder("songReport") // Specify the alias for the entity
        .select("songReport.song_id", "songId")
        .addSelect("SUM(songReport.count)", "likecount")
        .where("songReport.updatedAt >= NOW() - interval '7 day'")
        .andWhere("songReport.type = 2")
        .andWhere("songReport.song_id IN (:...songIds)", { songIds })
        .groupBy("songReport.song_id")
        .orderBy("likecount", "DESC") // Use the alias "totalCount" directly in orderBy
        .getRawMany();

      const songData = await songRepository
        .createQueryBuilder("song")
        .where("song.id IN (:...songIds)", { songIds })
        .andWhere("song.is_deleted = 0")
        .getMany();

      const response = trendingSongs?.map((song) => {
        const rs = data?.find((item) => item?.songId === song?.songId);
        const currentSOng = songData?.find((item) => item?.id === song?.songId);
        return {
          ...song,
          songName: currentSOng?.name,
          likecount: rs?.likecount,
        };
      });

      res.json(response);
    } else {
      res.json("error");
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};
