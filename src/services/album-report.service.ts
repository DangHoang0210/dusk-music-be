import { Request, Response } from "express";
import { jwtCongig } from "../config";
import { userRepository } from "../repository";
import jwt from "jsonwebtoken";
import { albumReportRepository } from "../repository/album-report.repository";

export const updateListenAlbumCount = async (req: Request, res: Response) => {
  try {
    const { albumId } = req.body;
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const existedRecord = await albumReportRepository.findOne({
            where: {
              user_id: user?.id,
              album_id: albumId,
              type: 1,
            },
          });
          if (existedRecord) {
            existedRecord.count += 1;
            existedRecord.album_id = Number(albumId);
            await albumReportRepository.save(existedRecord);
            res.json({
              status: 200,
              message: "Update listen count successfully!",
            });
          } else {
            const newReport = albumReportRepository.create({
              user_id: user.id,
              count: 1,
              type: 1,
              album_id: Number(albumId),
            });
            await albumReportRepository.save(newReport);
            res.json({
              status: 200,
              message: "Update listen count successfully!",
            });
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const toggleLikeAlbum = async (req: Request, res: Response) => {
  try {
    const { albumId } = req.body;
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const existedRecord = await albumReportRepository.findOne({
            where: {
              user_id: user?.id,
              album_id: albumId,
              type: 2,
            },
          });
          if (existedRecord) {
            existedRecord.is_deleted = existedRecord.is_deleted === 0 ? 1 : 0;
            await albumReportRepository.save(existedRecord);
            res.json("Update successfully !");
          } else {
            const newReport = albumReportRepository.create({
              user_id: user.id,
              count: 1,
              type: 2,
              album_id: Number(albumId),
            });
            await albumReportRepository.save(newReport);
            res.json("Update successfully !");
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};
