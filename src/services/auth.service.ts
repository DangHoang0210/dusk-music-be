import { generateAccessToken, generateRefreshToken } from "../jwt";
import { userRepository } from "../repository";
import { Request, Response } from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { jwtCongig } from "../config";

export const register = async (req: Request, res: Response) => {
  const { username, password } = req.body;
  const hashedPassword = await bcrypt.hash(password, 10);
  const user = await userRepository.findOne({ where: { username: username } });

  if (user) {
    res.json({ message: "Username existed!" });
  } else {
    const newUser = userRepository.create({
      username,
      name: username,
      is_admin: false,
      password: hashedPassword,
    });

    await userRepository.save(newUser);
    res.json({ status: 200, message: "Register successfully!" });
  }
};

export const changePassword = async (req: Request, res: Response) => {
  const { currentPass, newPassword } = req.body;

  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];

    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const isPasswordCorrect = await bcrypt.compare(
            currentPass,
            user.password
          );

          if (isPasswordCorrect) {
            const newHashedPassword = await bcrypt.hash(newPassword, 10);
            console.log("newHashedPassword: ", newHashedPassword);
            user.password = newHashedPassword;
            await userRepository.save(user);
            res.json({ message: "Change password successfully!" });
          } else {
            res.json({ message: "Account or password is incorrect!" });
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const updateUserInfor = async (req: Request, res: Response) => {
  const { name, userAvatar } = req.body;
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];

    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          user.name = name;
          user.avatar = userAvatar;
          await userRepository.save(user);
          const accessToken = generateAccessToken(user);
          const refreshToken = generateRefreshToken(user);
          res.json({ accessToken, refreshToken });
        } else {
          res.json({ message: "Failed!" });
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const registerAdminAccount = async (req: Request, res: Response) => {
  const { username, password } = req.body;
  const hashedPassword = await bcrypt.hash(password, 10);
  const user = await userRepository.findOne({ where: { username: username } });

  if (user) {
    res.json({ message: "Username existed!" });
  } else {
    const newUser = userRepository.create({
      username,
      name: username,
      is_admin: true,
      password: hashedPassword,
    });

    await userRepository.save(newUser);
    res.json({ status: 200, message: "Register successfully!" });
  }
};

export const login = async (req: Request, res: Response) => {
  const { username, password } = req.body;
  const user = await userRepository.findOne({ where: { username: username } });
  console.log("user: ", user);

  if (!user) {
    res.status(401).json({ message: "Invalid username or password" });
    return;
  }

  const isPasswordValid = await bcrypt.compare(password, user?.password);

  if (!isPasswordValid) {
    res.status(401).json({ message: "Invalid username or password" });
    return;
  }

  try {
    const accessToken = generateAccessToken(user);
    const refreshToken = generateRefreshToken(user);

    res.json({
      accessToken,
      refreshToken,
    });
  } catch (error) {
    console.log("error: ", error);
    res.status(412).send(error);
  }
};

export const loginAdmin = async (req: Request, res: Response) => {
  const { username, password } = req.body;
  const user = await userRepository.findOne({
    where: { username: username, is_admin: true },
  });

  if (!user) {
    res.status(401).json({ message: "Invalid username or password" });
    return;
  }

  const isPasswordValid = await bcrypt.compare(password, user?.password);

  if (!isPasswordValid) {
    res.status(401).json({ message: "Invalid username or password" });
    return;
  }

  try {
    const accessToken = generateAccessToken(user);
    const refreshToken = generateRefreshToken(user);

    res.json({
      accessToken,
      refreshToken,
    });
  } catch (error) {
    console.log("error: ", error);
    res.status(412).send(error);
  }
};
