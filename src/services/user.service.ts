import { generateAccessToken, generateRefreshToken } from "../jwt";
import { userRepository } from "../repository";
import { Request, Response } from "express";

export const login = async (req: Request, res: Response) => {
  const data = req.body;
  try {
    const user = await userRepository.findByUsername(data.username);
    const accessToken = generateAccessToken(user?.username);
    const refreshToken = generateRefreshToken(user?.username);
    res.json({ accessToken, refreshToken });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getUsers = async (req: Request, res: Response) => {
  const { limit, offset } = req.query;
  const total = await userRepository.count();

  try {
    const users = await userRepository.find({
      order: {
        id: "DESC",
      },
      take: Number(limit),
      skip: Number(offset),
    });

    res.json({
      users: users,
      total: total,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getNewUsers = async (req: Request, res: Response) => {
  const { limit, offset } = req.query;
  const total = await userRepository.count();

  try {
    const users = await userRepository.find({
      order: {
        createdAt: "DESC",
      },
      take: Number(limit),
      skip: Number(offset),
    });

    res.json({
      users: users,
      total: total,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};
