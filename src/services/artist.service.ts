import { Request, Response } from "express";
import { artistRepository } from "../repository/artist.repository";
import jwt from "jsonwebtoken";
import { jwtCongig } from "../config";
import { userRepository } from "../repository";
import { getLikeState, getSongReport } from "./song.service";

export const getArtists = async (req: Request, res: Response) => {
  const { limit, offset } = req.query;
  const total = await artistRepository
    .createQueryBuilder("artist")
    .where("artist.is_deleted = 0")
    .getCount();
  try {
    const artists = await artistRepository.find({
      order: {
        id: "DESC",
      },
      where: {
        is_deleted: 0,
      },
      take: Number(limit),
      skip: Number(offset),
    });

    res.json({
      artists,
      total,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getNewArtitst = async (req: Request, res: Response) => {
  try {
    const newestArtists = await artistRepository
      .createQueryBuilder("artist")
      .leftJoinAndSelect("artist.songs", "song")
      .orderBy("artist.createdAt", "DESC")
      .take(5)
      .getMany();

    const artistsWithTotalSongs = newestArtists.map((artist) => {
      const totalSongs = artist.songs.length;
      return { ...artist, totalSongs };
    });

    res.json({
      artists: artistsWithTotalSongs,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const createNewArtist = async (req: Request, res: Response) => {
  const { artistName, artistAvatar } = req.body;
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const newArtist = artistRepository.create({
            avatar: artistAvatar,
            name: artistName,
          });

          artistRepository.save(newArtist);
          res.json(newArtist);
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const updateArtist = async (req: Request, res: Response) => {
  const { id, artistName, artistAvatar } = req.body;
  try {
    const currentArtist = await artistRepository.findOne({
      where: {
        id,
        is_deleted: 0,
      },
    });

    if (currentArtist) {
      currentArtist.name = artistName;
      currentArtist.avatar = artistAvatar;
      await artistRepository.save(currentArtist);
      res.json("Update successfully !");
    } else {
      res.json("Artist not found!");
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const deleteArtist = async (req: Request, res: Response) => {
  const { id } = req.body;
  try {
    const currentArtist = await artistRepository.findOne({
      where: {
        id,
        is_deleted: 0,
      },
    });

    if (currentArtist) {
      currentArtist.is_deleted = 1;
      await artistRepository.save(currentArtist);
      res.json("Delete successfully !");
    } else {
      res.json("Category not found!");
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getCurrentArtist = async (req: Request, res: Response) => {
  const { artistId } = req.params;
  const artist = await artistRepository.findOne({
    where: {
      id: Number(artistId),
    },
  });

  try {
    if (artist) {
      res.json({
        ...artist,
      });
    } else {
      res.status(404).json({
        message: "Artist not found.",
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getSongFromArtist = async (req: Request, res: Response) => {
  const { artistId } = req.params;
  const artist = await artistRepository
    .createQueryBuilder("artist")
    .where("artist.id=:id", { id: artistId })
    .leftJoinAndSelect("artist.songs", "songs")
    .leftJoinAndSelect("songs.artists", "artists")
    .getOne();
  try {
    if (artist) {
      const songIds = artist?.songs?.map((song) => song?.id);
      const listenState = await getSongReport(songIds);
      const songState = await getLikeState(songIds);

      const data = artist?.songs?.map((song) => {
        const songReport = listenState?.find((item) => item?.id === song?.id);
        const likedState = songState?.find((item) => item?.id === song?.id);
        return {
          ...song,
          likeCount: songReport?.likeCount,
          listenCount: songReport?.listenTotal,
          isLiked: likedState?.isLiked,
        };
      });

      res.json({
        ...artist,
        songs: data,
      });
    } else {
      res.status(404).json({
        message: "Artist not found.",
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getAlbumFromArtist = async (req: Request, res: Response) => {
  try {
    const { artistId } = req.params;

    const artist = await artistRepository.findOne({
      where: {
        id: Number(artistId),
        is_deleted: 0,
      },
      relations: ["albums", "albums.songs"],
    });

    if (artist) {
      const albums = artist.albums;

      // Optionally, you can map the albums to include only necessary information
      const formattedAlbums = albums.map((album) => {
        const currentImage = !!album?.imageUrl
          ? album?.imageUrl
          : album?.songs?.length > 0
          ? album?.songs?.[0]?.imageUrl
          : null;
        return {
          ...album,
          imageUrl: currentImage,
          songs: album.songs.map((song) => ({
            id: song.id,
            name: song.name,
            // Add other song properties as needed
          })),
        };
      });

      res.json({
        albums: formattedAlbums,
      });
    } else {
      res.status(404).json({
        message: "Artist not found.",
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send(error);
  }
};
