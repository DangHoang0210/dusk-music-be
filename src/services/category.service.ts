import { Request, Response } from "express";
import { categoryRepository } from "../repository/category.repository";

export const getCategories = async (req: Request, res: Response) => {
  const { limit, offset } = req.query;
  const total = await categoryRepository
    .createQueryBuilder("category")
    .where("category.is_deleted = 0")
    .getCount();
  try {
    const categories = await categoryRepository.find({
      order: {
        id: "ASC",
      },
      where: {
        is_deleted: 0,
      },
      take: Number(limit),
      skip: Number(offset),
    });

    res.json({
      categories: categories,
      total: total,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getUserCategories = async (req: Request, res: Response) => {
  try {
    const response = await categoryRepository
      .createQueryBuilder("category")
      .leftJoin("category.songs", "song")
      .addSelect("COUNT(song.id)", "songCount")
      .where("category.is_deleted = :isDeleted", { isDeleted: 0 })
      .groupBy(
        "category.id, category.name, category.backgroundImage, category.description, category.is_deleted"
      )
      .getRawMany();

    const categories = response?.filter(
      (category) => Number(category?.songCount) > 0
    );

    res.json(categories);
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const createCategory = async (req: Request, res: Response) => {
  const { categoryName, categoryDesc, categoryBg } = req.body;
  try {
    const newCategory = categoryRepository.create({
      name: categoryName,
      description: categoryDesc,
      backgroundImage: categoryBg,
    });

    categoryRepository.save(newCategory);
    res.json(newCategory);
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const updateCategory = async (req: Request, res: Response) => {
  const { id, categoryName, categoryDesc, categoryBg } = req.body;
  try {
    const currentCategory = await categoryRepository.findOne({
      where: {
        id,
        is_deleted: 0,
      },
    });

    if (currentCategory) {
      currentCategory.name = categoryName;
      currentCategory.description = categoryDesc;
      currentCategory.backgroundImage = categoryBg;
      await categoryRepository.save(currentCategory);
      res.json("Update successfully !");
    } else {
      res.json("Category not found!");
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const deleteCategory = async (req: Request, res: Response) => {
  const { id } = req.body;
  try {
    const currentCategory = await categoryRepository.findOne({
      where: {
        id,
        is_deleted: 0,
      },
    });

    if (currentCategory) {
      currentCategory.is_deleted = 1;
      await categoryRepository.save(currentCategory);
      res.json("Delete successfully !");
    } else {
      res.json("Category not found!");
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const categoriesWithTotalSongs = async (req: Request, res: Response) => {
  try {
    const categories = await categoryRepository
      .createQueryBuilder("category")
      .where("category.is_deleted = 0")
      .leftJoinAndSelect("category.songs", "song")
      .addSelect("COUNT(song.id)", "totalSongs")
      .groupBy("category.id, song.id") // Include song.id in the GROUP BY clause
      .take(5)
      .getMany();

    const data = categories?.map((category) => {
      return {
        ...category,
        totalSongs: category?.songs?.length,
      };
    });

    res.json({
      categories: data,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};
