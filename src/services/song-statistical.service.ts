import { Request, Response } from "express";
import { artistRepository } from "../repository/artist.repository";
import { albumRepository } from "../repository/album.repository";
import jwt from "jsonwebtoken";
import { jwtCongig } from "../config";
import { userRepository } from "../repository";
import { songRepository } from "../repository/song.repository";
import { albumReportRepository } from "../repository/album-report.repository";
import { getSongReport, getLikeState as getFavSongState } from "./song.service";
import { songStatistialRepository } from "../repository/song-statistical.repository";

export const getAlbums = async (req: Request, res: Response) => {
  const page = req.query.page ? parseInt(req.query.page as string, 10) : 1;
  const { size } = req.params ?? 10;
  const limit = 12;
  const offset = (page - 1) * limit;
  const total = await albumRepository
    .createQueryBuilder("album")
    .where("album.is_deleted = 0")
    .getCount();

  try {
    const albums = await albumRepository.find({
      order: {
        createdAt: "DESC",
      },
      where: {
        is_deleted: 0,
      },
      take: limit,
      skip: offset,
      relations: ["artist"],
    });

    const responseItems = albums.map((album) => {
      return {
        ...album,
      };
    });

    res.json({
      items: responseItems,
      total: total,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

const getTotalListenByDay = async (day: Date) => {
  try {
    const currentMonth = day.getMonth() + 1;
    const currentYear = day.getFullYear();
    const currentDate = day.getDate();

    day.setUTCHours(0, 0, 0, 0);
    const songStatiscial = await songStatistialRepository
      .createQueryBuilder("songStatistical")
      .select("SUM(songStatistical.count)", "totalCount")
      .where(`EXTRACT(MONTH FROM songStatistical.date) = :currentMonth`, {
        currentMonth,
      })
      .andWhere(`EXTRACT(YEAR FROM songStatistical.date) = :currentYear`, {
        currentYear,
      })
      .andWhere(`EXTRACT(DAY FROM songStatistical.date) = :currentDate`, {
        currentDate,
      })
      .getRawOne();

    return Number(songStatiscial.totalCount) ?? 0;
  } catch (error) {
    console.error("Error calculating total points:", error);
    throw error;
  }
};

const getLastSevenDays = () => {
  const today = new Date();
  const lastFiveDays = [];

  for (let i = 0; i < 7; i++) {
    const currentDate = new Date(today);
    currentDate.setDate(today.getDate() - i);
    lastFiveDays.push(currentDate);
  }

  return lastFiveDays;
};

export const calculateTotalListenByUser = async (
  req: Request,
  res: Response
) => {
  try {
    const dayArr = getLastSevenDays();
    const result = [];
    for (let i = 0; i < dayArr?.length; i++) {
      const rs = await getTotalListenByDay(dayArr[i]);
      result.push(rs);
    }

    res.json(result);
  } catch (error) {
    console.error("Error calculating total points:", error);
    throw error;
  }
};
