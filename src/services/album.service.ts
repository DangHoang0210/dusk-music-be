import { Request, Response } from "express";
import { artistRepository } from "../repository/artist.repository";
import { albumRepository } from "../repository/album.repository";
import jwt from "jsonwebtoken";
import { jwtCongig } from "../config";
import { userRepository } from "../repository";
import { songRepository } from "../repository/song.repository";
import { albumReportRepository } from "../repository/album-report.repository";
import { getSongReport, getLikeState as getFavSongState } from "./song.service";

export const getNewReleaseAlbums = async (req: Request, res: Response) => {
  const { limit, offset } = req.query;

  const total = await albumRepository.count();

  try {
    const albums = await albumRepository.find({
      order: {
        createdAt: "DESC",
      },
      where: {
        is_deleted: 0,
      },
      take: Number(limit),
      skip: Number(offset),
      relations: ["artist", "songs"],
    });

    const albumsIds = albums?.map((album) => album?.id);
    const rs = await getLikeState(albumsIds);
    const responseItems = albums.map((album) => {
      const likedState = rs?.find((item) => item?.id === album?.id);
      return {
        ...album,
        songTotal: album?.songs?.length,
        isLiked: likedState?.isLiked,
      };
    });

    res.json({
      items: responseItems,
      total: total,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getAlbums = async (req: Request, res: Response) => {
  const page = req.query.page ? parseInt(req.query.page as string, 10) : 1;
  const { size } = req.params ?? 10;
  const limit = 12;
  const offset = (page - 1) * limit;
  const total = await albumRepository
    .createQueryBuilder("album")
    .where("album.is_deleted = 0")
    .getCount();

  try {
    const albums = await albumRepository.find({
      order: {
        createdAt: "DESC",
      },
      where: {
        is_deleted: 0,
      },
      take: limit,
      skip: offset,
      relations: ["artist"],
    });

    const responseItems = albums.map((album) => {
      return {
        ...album,
      };
    });

    res.json({
      items: responseItems,
      total: total,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const createAlbum = async (req: Request, res: Response) => {
  const { albumName, albumDesc, artistId } = req.body;
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        const artist = await artistRepository.findOne({
          where: {
            id: Number(artistId),
            is_deleted: 0,
          },
        });

        if (user) {
          if (artist) {
            const newAlbum = albumRepository.create({
              artist: artist,
              name: albumName,
              description: albumDesc,
            });
            albumRepository.save(newAlbum);
            res.json(newAlbum);
          } else {
            res.json("Err");
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const updateAlbum = async (req: Request, res: Response) => {
  const { id, albumName, albumDesc } = req.body;
  try {
    const currentAlbum = await albumRepository.findOne({
      where: {
        id,
        is_deleted: 0,
      },
    });

    if (currentAlbum) {
      currentAlbum.name = albumName;
      currentAlbum.description = albumDesc;
      await albumRepository.save(currentAlbum);
      res.json("Update successfully !");
    } else {
      res.json("Album not found!");
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const removeAlbum = async (req: Request, res: Response) => {
  const { albumId } = req.body;
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const album = await albumRepository
            .createQueryBuilder("album")
            .where("album.id = :id", { id: albumId })
            .andWhere("album.is_deleted = 0")
            .getOne();

          if (album) {
            album.is_deleted = 1;
            albumRepository.save(album);
            res.json({
              status: 200,
              message: "Remove this album successfully!",
            });
          } else {
            res.json({ status: 404, message: "Album not found!" });
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getAlbumFromArtist = async (req: Request, res: Response) => {
  const page = req.query.page ? parseInt(req.query.page as string, 10) : 1;
  const { artistId } = req.params;
  const limit = 9;
  const offset = (page - 1) * limit;
  // const total = await songRepository.count();
  const artist = await artistRepository.findOne({
    where: {
      id: Number(artistId),
    },
    relations: ["songs"],
  });

  try {
    if (artist) {
      const songs = artist.songs;

      res.json({
        ...artist,
        songs,
      });
    } else {
      res.status(404).json({
        message: "Artist not found.",
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

//ADMIN
export const addSongToAlbum = async (req: Request, res: Response) => {
  const { songId, albumId } = req.body;
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const song = await songRepository
            .createQueryBuilder("song")
            .where("song.id = :id", { id: songId })
            .andWhere("song.is_deleted = 0")
            .getOne();

          const album = await albumRepository
            .createQueryBuilder("album")
            .leftJoinAndSelect("album.songs", "songs")
            .where("album.id = :id", { id: albumId })
            .andWhere("album.is_deleted = 0")
            .getOne();

          if (!song || !album) {
            res.json({ message: "Add failed" });
          } else {
            const existedSong = album?.songs.filter(
              (item) => item?.id === Number(songId)
            );
            if (existedSong.length > 0) {
              res.status(400).send("Song existed in album!");
            } else {
              album?.songs.push(song);
              await albumRepository.save(album);
              res.status(200).send("Song added to album successfully!");
            }
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const removeSongFromAlbum = async (req: Request, res: Response) => {
  const { songId, albumId } = req.body;
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const song = await songRepository
            .createQueryBuilder("song")
            .where("song.id = :id", { id: songId })
            .andWhere("song.is_deleted = 0")
            .getOne();

          const album = await albumRepository
            .createQueryBuilder("album")
            .leftJoinAndSelect("album.songs", "songs")
            .where("album.id = :id", { id: albumId })
            .andWhere("album.is_deleted = 0")
            .getOne();

          if (!album) {
            res.json({ message: "Remove failed" });
          } else {
            if (song) {
              const existedSong = album?.songs.filter(
                (item) => item?.id === Number(songId)
              );

              if (existedSong.length > 0) {
                album.songs = album.songs.filter((s) => s.id !== song.id);
                await albumRepository.save(album);
                res.status(200).send("Remove song from album!");
              } else {
                res.status(404).send("Song not existed in album!");
              }
            } else {
              res.status(404).send("Song not found!");
            }
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getFavouriteAlbums = async (req: Request, res: Response) => {
  const { limit, offset } = req.query;

  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const albumReports = await albumReportRepository
            .createQueryBuilder("albumReport")
            .where("albumReport.user_id = :id", { id: user?.id })
            .andWhere("albumReport.is_deleted = 0")
            .andWhere("albumReport.type = 2")
            .take(Number(limit))
            .skip(Number(offset))
            .getMany();

          const albumTotal = await albumReportRepository
            .createQueryBuilder("albumReport")
            .where("albumReport.user_id = :id", { id: user?.id })
            .andWhere("albumReport.is_deleted = 0")
            .andWhere("albumReport.type = 2")
            .getCount();
          const favouriteAlbumIds = albumReports.map((item) => item?.album_id);

          const data = await albumRepository
            .createQueryBuilder("album")
            .leftJoinAndSelect("album.artist", "artist")
            .leftJoinAndSelect("album.songs", "songs")
            .where("album.id IN (:...albumIds)", {
              albumIds: favouriteAlbumIds,
            })
            .andWhere("album.is_deleted = 0")
            .getMany();

          const likeState = await getLikeState(favouriteAlbumIds);

          const rs = data.map((album) => {
            const likeResponse = likeState?.find(
              (item) => item?.id === album?.id
            );
            return {
              ...album,
              imageUrl: album.songs[0]?.imageUrl,
              isLiked: likeResponse?.isLiked,
              // artists: album.artists.map((artist) => ({
              //   id: artist.id,
              //   name: artist.name,
              // })),
            };
          });

          res.json({
            items: rs,
            total: albumTotal,
          });
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const getLikeState = async (albumIds: number[]) => {
  const albumReports = await albumReportRepository
    .createQueryBuilder("albumReport")
    .where("albumReport.album_id IN (:...albumIds)", { albumIds })
    .andWhere("albumReport.is_deleted = 0")
    .andWhere("albumReport.type = 2")
    .getMany();

  const rs = albumIds?.map((id) => {
    const likedState = albumReports?.find((album) => album?.album_id === id);
    return {
      id,
      isLiked: likedState ? true : false,
    };
  });

  return rs;
};

export const getAlbum = async (req: Request, res: Response) => {
  const { albumId } = req.params;
  try {
    const data = await albumRepository
      .createQueryBuilder("album")
      .leftJoinAndSelect("album.songs", "songs")
      .where("album.is_deleted = 0")
      .andWhere("album.id = :id", { id: albumId })
      .getOne();

    if (!!data && data?.songs?.length > 0) {
      const songIds = data?.songs?.map((item) => item?.id);
      const listenState = await getSongReport(songIds);
      const songState = await getFavSongState(songIds);

      const albumDataResponse = data?.songs?.map((song) => {
        const songReport = listenState?.find((item) => item?.id === song?.id);
        const likedState = songState?.find((item) => item?.id === song?.id);
        return {
          ...song,
          artists: song?.artists?.map((artist) => ({
            id: artist.id,
            name: artist.name,
          })),
          likeCount: songReport?.likeCount,
          listenCount: songReport?.listenTotal,
          isLiked: likedState?.isLiked,
        };
      });

      const rs = await getLikeState([Number(albumId)]);
      res.json({
        data: {
          ...data,
          songs: albumDataResponse,
          isLiked: rs?.[0]?.isLiked ? true : false,
        },
        total: data?.songs.length,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};
