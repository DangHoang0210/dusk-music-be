import { userRepository } from "../repository";
import { Request, Response } from "express";
import { playlistRepository } from "../repository/playlist.repository";
import jwt from "jsonwebtoken";
import { jwtCongig } from "../config";
import { songRepository } from "../repository/song.repository";

export const getPlaylists = async (req: Request, res: Response) => {
  const { limit, offset } = req.query;
  const total = await playlistRepository
    .createQueryBuilder("playlist")
    .where("playlist.is_deleted = 0")
    .getCount();

  try {
    const playlists = await playlistRepository.find({
      order: {
        id: "ASC",
      },
      where: {
        is_deleted: 0,
      },
      take: Number(limit),
      skip: Number(offset),
      relations: ["songs", "user"],
    });

    res.json({
      playlists,
      total,
    });
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const createPlaylist = async (req: Request, res: Response) => {
  const { playlistName, playlistDesc } = req.body;
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
          relations: ["playlists"],
        });

        if (user) {
          const newPlaylist = playlistRepository.create({
            user_id: user?.id,
            name: playlistName,
            description: playlistDesc,
          });

          playlistRepository.save(newPlaylist);
          res.json(newPlaylist);
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const updatePlaylist = async (req: Request, res: Response) => {
  const { id, playlistName, playlistDesc } = req.body;
  try {
    const currentPlaylist = await playlistRepository.findOne({
      where: {
        id,
        is_deleted: 0,
      },
    });

    if (currentPlaylist) {
      currentPlaylist.name = playlistName;
      currentPlaylist.description = playlistDesc;
      await playlistRepository.save(currentPlaylist);
      res.json("Update successfully !");
    } else {
      res.json("Playlist not found!");
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const removePlaylist = async (req: Request, res: Response) => {
  const { playlistId } = req.body;
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const playlist = await playlistRepository
            .createQueryBuilder("playlist")
            .where("playlist.id = :id", { id: playlistId })
            .andWhere("playlist.is_deleted = 0")
            .getOne();

          if (playlist) {
            playlist.is_deleted = 1;
            playlistRepository.save(playlist);
            res.json({
              status: 200,
              message: "Remove this playlist successfully!",
            });
          } else {
            res.json({ status: 404, message: "Playlist not found!" });
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const addSongToPlaylist = async (req: Request, res: Response) => {
  const { songId, playlistId } = req.body;
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
          relations: ["playlists"],
        });

        if (user) {
          const song = await songRepository
            .createQueryBuilder("song")
            .where("song.id = :id", { id: songId })
            .andWhere("song.is_deleted = 0")
            .getOne();

          const playlist = await playlistRepository
            .createQueryBuilder("playlist")
            .leftJoinAndSelect("playlist.songs", "songs")
            .where("playlist.id = :id", { id: playlistId })
            .andWhere("playlist.is_deleted = 0")
            .getOne();

          if (!song || !playlist) {
            res.json({ message: "Add failed" });
          } else {
            const existedSong = playlist?.songs.filter(
              (item) => item?.id === Number(songId)
            );
            if (existedSong.length > 0) {
              res.status(400).send("Song existed in playlist!");
            } else {
              playlist?.songs.push(song);
              await playlistRepository.save(playlist);
              res.status(200).send("Song added to playlist successfully!");
            }
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const removeSongFromPlaylist = async (req: Request, res: Response) => {
  const { songId, playlistId } = req.body;
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
          relations: ["playlists"],
        });

        if (user) {
          const song = await songRepository
            .createQueryBuilder("song")
            .where("song.id = :id", { id: songId })
            .andWhere("song.is_deleted = 0")
            .getOne();

          const playlist = await playlistRepository
            .createQueryBuilder("playlist")
            .leftJoinAndSelect("playlist.songs", "songs")
            .where("playlist.id = :id", { id: playlistId })
            .andWhere("playlist.is_deleted = 0")
            .getOne();

          if (!playlist) {
            res.json({ message: "Remove failed" });
          } else {
            if (song) {
              const existedSong = playlist?.songs.filter(
                (item) => item?.id === Number(songId)
              );

              if (existedSong.length > 0) {
                playlist.songs = playlist.songs.filter((s) => s.id !== song.id);
                await playlistRepository.save(playlist);
                res.status(200).send("Remove song from playlist!");
              } else {
                res.status(404).send("Song not existed in playlist!");
              }
            } else {
              res.status(404).send("Song not found!");
            }
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getUserPlaylist = async (req: Request, res: Response) => {
  // const { limit, offset } = req.query;
  try {
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const playlists = await playlistRepository.find({
            order: {
              id: "ASC",
            },
            where: {
              user_id: user?.id,
              is_deleted: 0,
            },
            relations: ["songs"],

            // take: Number(limit),
            // skip: Number(offset),
          });
          res.json(playlists);
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};
