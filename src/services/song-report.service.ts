import { Request, Response } from "express";
import { jwtCongig } from "../config";
import { userRepository } from "../repository";
import jwt from "jsonwebtoken";
import { songReportRepository } from "../repository/song-report.repository";
import { songStatistialRepository } from "../repository/song-statistical.repository";
import { Between } from "typeorm";
import { playlistRepository } from "../repository/playlist.repository";
import { songRepository } from "../repository/song.repository";
import { albumRepository } from "../repository/album.repository";
import { artistRepository } from "../repository/artist.repository";
import { categoryRepository } from "../repository/category.repository";

export const updateSongListen = async (req: Request, res: Response) => {
  try {
    const { songId } = req.body;
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const existedRecord = await songReportRepository.findOne({
            where: {
              user_id: user?.id,
              song_id: songId,
              type: 1,
            },
          });
          const currentDate = new Date();
          currentDate.setHours(0, 0, 0, 0);
          const existedStatisticalRecord =
            await songStatistialRepository.findOne({
              where: {
                user_id: user?.id,
                song_id: songId,
                date: Between(currentDate, new Date(new Date().toISOString())),
              },
            });

          if (!existedStatisticalRecord) {
            const newStatistical = songStatistialRepository.create({
              user_id: user?.id,
              song_id: Number(songId),
              date: new Date(new Date()),
              count: 1,
              is_deleted: 0,
            });

            await songStatistialRepository.save(newStatistical);
          } else {
            existedStatisticalRecord.count += 1;
            existedStatisticalRecord.song_id = Number(songId);
            await songStatistialRepository.save(existedStatisticalRecord);
          }

          if (existedRecord) {
            existedRecord.count += 1;
            existedRecord.song_id = Number(songId);
            await songReportRepository.save(existedRecord);
            res.json({
              status: 200,
              message: "Update listen count successfully!",
            });
          } else {
            const newReport = songReportRepository.create({
              user_id: user.id,
              count: 1,
              type: 1,
              song_id: Number(songId),
            });

            await songReportRepository.save(newReport);
            res.json({
              status: 200,
              message: "Update listen count successfully!",
            });
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const toggleLikeSong = async (req: Request, res: Response) => {
  try {
    const { songId } = req.body;
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const existedRecord = await songReportRepository.findOne({
            where: {
              user_id: user?.id,
              song_id: songId,
              type: 2,
            },
          });
          if (existedRecord) {
            existedRecord.is_deleted = existedRecord.is_deleted === 0 ? 1 : 0;
            await songReportRepository.save(existedRecord);
            res.json("Update successfully !");
          } else {
            const newReport = songReportRepository.create({
              user_id: user.id,
              count: 1,
              type: 2,
              song_id: Number(songId),
            });
            await songReportRepository.save(newReport);
            res.json("Update successfully !");
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getCurrent = async (req: Request, res: Response) => {
  try {
    const { type, songId } = req.body;
    const authorizationHeader = req.headers["authorization"];
    const token = authorizationHeader?.split(" ")[1];
    if (!token) {
      res.sendStatus(401);
    } else {
      jwt.verify(token, jwtCongig?.jwtSecret, async (err, resp: any) => {
        const user = await userRepository.findOne({
          where: {
            id: resp?.data?.id,
          },
        });

        if (user) {
          const existedRecord = await songReportRepository.findOne({
            where: {
              user_id: user?.id,
              song_id: songId,
              type: type,
            },
          });
          if (existedRecord) {
            existedRecord.is_deleted = existedRecord.is_deleted === 0 ? 1 : 0;
            await songReportRepository.save(existedRecord);
          } else {
            const newReport = songReportRepository.create({
              user_id: user.id,
              count: 1,
              type: Number(type),
              song_id: Number(songId),
            });
            await songReportRepository.save(newReport);
            res.json("Update successfully !");
          }
        }
        if (err) resp?.data.sendStatus(403);
      });
    }
  } catch (error) {
    console.log(error);
    res.status(412).send(error);
  }
};

export const getAllStatistical = async (req: Request, res: Response) => {
  const songCount = await songRepository
    .createQueryBuilder("song")
    .where("song.is_deleted = 0")
    .getCount();
  const albumCount = await albumRepository
    .createQueryBuilder("album")
    .where("album.is_deleted = 0")
    .getCount();
  const artistCount = await artistRepository
    .createQueryBuilder("artist")
    .where("artist.is_deleted = 0")
    .getCount();
  const playlistCount = await playlistRepository
    .createQueryBuilder("playlist")
    .where("playlist.is_deleted = 0")
    .getCount();
  const userCount = await userRepository
    .createQueryBuilder("user")
    .where("user.is_deleted = 0")
    .getCount();

  const categoryCount = await categoryRepository
    .createQueryBuilder("category")
    .where("category.is_deleted = 0")
    .getCount();

  res.json({
    songCount,
    albumCount,
    artistCount,
    playlistCount,
    userCount,
    categoryCount,
  });
};
