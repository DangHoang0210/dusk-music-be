import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { User } from "./user.entity";
import { Category } from "./category.entity";
import { Artist } from "./artist.entity";
import { Album } from "./album.entity";
import { Playlist } from "./playlist.entity";

@Entity()
export class Song {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.songs)
  @JoinColumn({ name: "created_by" })
  user: User;
  @Column({ name: "created_by", nullable: true })
  created_by: number;

  @Column()
  name: string;

  @Column({ nullable: true })
  lyric: string;

  @Column({ nullable: true })
  imageUrl: string;

  @ManyToMany(() => Category, (category) => category.songs)
  @JoinTable()
  categories: Category[];

  @ManyToMany(() => Artist, (artist) => artist.songs)
  @JoinTable()
  artists: Artist[];

  @ManyToMany(() => Album, (album) => album.songs)
  @JoinTable()
  albums: Album[];

  @ManyToMany(() => Playlist, (playlist) => playlist.songs)
  @JoinTable()
  playlists: Playlist[];

  @Column({ nullable: false })
  link: string;

  @Column({ nullable: false })
  duration: number;

  @Column({ default: 0 })
  is_deleted: number;

  @Column({ default: 0 })
  status: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
