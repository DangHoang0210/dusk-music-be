import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Song } from "./song.entity";

@Entity()
export class Category {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(() => Song, (song) => song.categories)
  songs: Song[];

  @Column()
  name: string;

  @Column({ nullable: true })
  backgroundImage: string;

  @Column({ nullable: true })
  description: string;

  @Column({ default: 0 })
  is_deleted: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
