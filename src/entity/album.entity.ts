import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Song } from "./song.entity";
import { Artist } from "./artist.entity";

@Entity()
export class Album {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Artist, (artist) => artist.id)
  artist: Artist;

  @ManyToMany(() => Song, (song) => song.albums)
  songs: Song[];

  @Column({ nullable: true })
  imageUrl: string;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column({ default: 0 })
  is_deleted: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
