import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity()
export class SongStatiscial {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: number;

  @Column()
  song_id: number;

  @Column()
  count: number;

  @Column({ default: 0 })
  is_deleted: number;

  @Column({
    type: "timestamptz", // 'timestamptz' is used for storing timestamps with time zone information
    default: () => "CURRENT_TIMESTAMP",
  })
  date: Date;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
