import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Song } from "./song.entity";
import { Album } from "./album.entity";

@Entity()
export class Artist {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(() => Song, (song) => song.artists)
  songs: Song[];

  @OneToMany(() => Album, (album) => album.artist)
  albums: Album[];

  @Column()
  name: string;

  @Column({ nullable: true })
  bio: string;

  @Column({ nullable: true })
  avatar: string;

  @Column({ nullable: true })
  background_profile: string;

  @Column({ default: 0 })
  is_deleted: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
