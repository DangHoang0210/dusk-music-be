import * as dotenv from "dotenv";
dotenv.config();

import express, { NextFunction, Request, Response } from "express";
import * as bodyParser from "body-parser";
import cors from "cors";
import * as http from "http";
import { AppDataSource } from "./connector/postgres";
import { getNewUsers, getUsers } from "./services/user.service";
import { authenToken, refreshToken } from "./jwt";
import {
  changePassword,
  login,
  loginAdmin,
  register,
  registerAdminAccount,
  updateUserInfor,
} from "./services/auth.service";
import {
  createNewSong,
  createNewUserSong,
  deleteSong,
  deleteUserSong,
  getAudio,
  getCurrentSong,
  getFavouriteSongs,
  getImages,
  getNewReleaseSongs,
  getRecentListen,
  getSongs,
  getSongsByCategories,
  getSongsFromPlaylist,
  getStatisticalByDay,
  getTrendingSongs,
  getUserUploadedSongs,
  searchByKeyword,
  updateSong,
  updateUploadedSong,
  uploadSongAudioAdmin,
  uploadSongThumbAdmin,
} from "./services/song.service";
import {
  createNewArtist,
  deleteArtist,
  getAlbumFromArtist,
  getArtists,
  getCurrentArtist,
  getNewArtitst,
  getSongFromArtist,
  updateArtist,
} from "./services/artist.service";
import {
  addSongToAlbum,
  createAlbum,
  getAlbum,
  getAlbums,
  getFavouriteAlbums,
  getNewReleaseAlbums,
  removeAlbum,
  removeSongFromAlbum,
  updateAlbum,
} from "./services/album.service";
import multer from "multer";
import path from "path";
import {
  addSongToPlaylist,
  createPlaylist,
  getPlaylists,
  getUserPlaylist,
  removePlaylist,
  removeSongFromPlaylist,
  updatePlaylist,
} from "./services/playlist.service";
import {
  getAllStatistical,
  toggleLikeSong,
  updateSongListen,
} from "./services/song-report.service";
import {
  categoriesWithTotalSongs,
  createCategory,
  deleteCategory,
  getCategories,
  getUserCategories,
  updateCategory,
} from "./services/category.service";
import {
  toggleLikeAlbum,
  updateListenAlbumCount,
} from "./services/album-report.service";
import {
  toggleLikePlaylist,
  updateListenPlaylistCount,
} from "./services/playlist-report.service";
import { calculateTotalListenByUser } from "./services/song-statistical.service";

const app = express();

console.log("Pid: %s", process.pid);

const corsOptions = {
  origin: "*",
};

app.use(cors(corsOptions));
app.use(bodyParser.json());

AppDataSource.initialize()
  .then((ds) => {
    console.log("DB running");
    // console.log(ds);
  })
  .catch((error) => console.log(error));

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const uploadPath = path.join("./", "uploads/"); // Construct the absolute path
    cb(null, uploadPath); // Directory where audio files will be saved
  },
  filename: (req, file, cb) => {
    cb(
      null,
      `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`
    );
  },
});

const audioStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    const uploadPath = path.join("./", "uploads/audios"); // Construct the absolute path
    cb(null, uploadPath); // Directory where audio files will be saved
  },
  filename: (req, file, cb) => {
    cb(
      null,
      `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`
    );
  },
});

const imageStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    const uploadPath = path.join("./", "uploads/images"); // Construct the absolute path
    cb(null, uploadPath); // Directory where audio files will be saved
  },
  filename: (req, file, cb) => {
    cb(
      null,
      `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`
    );
  },
});

const upload = multer({ storage });
const uploadAudios = multer({ storage: audioStorage });
const uploadImages = multer({ storage: imageStorage });

app.post("/api/login", login);
app.post("/api/admin/login", loginAdmin);
app.post("/api/refresh-token", refreshToken);
app.post("/api/register", register);
app.post("/api/change-password", changePassword);
app.post("/api/update-user", updateUserInfor);
app.post("/api/admin/register", registerAdminAccount);
app.get("/api/users", getUsers);
app.get("/api/new-users", getNewUsers);
app.get("/api/user", authenToken);

// SONG
app.get("/api/release-songs", getNewReleaseSongs);
app.get("/api/admin/songs", getSongs);
app.get("/api/current-song", getCurrentSong);

app.post("/api/admin/update-song", updateSong);
app.post("/api/update-uploaded-song", updateUploadedSong);
app.get("/api/favourite-songs", getFavouriteSongs);
app.get("/api/song/category", getSongsByCategories);
app.get("/api/audio/:audioId", getAudio);
app.get("/api/image/:imageId", getImages);
app.post("/api/update-song-report", updateSongListen);
app.post("/api/toggle-like-song", toggleLikeSong);
app.post("/api/delete-song", deleteSong);
app.post("/api/delete-user-song", deleteUserSong);
app.get("/api/trending-songs", getTrendingSongs);
app.get("/api/user-uploaded-songs", getUserUploadedSongs);

app.get("/api/calculate-listen-song", calculateTotalListenByUser);

//ALBUM
app.get("/api/admin/albums", getAlbums);
app.get("/api/release-albums", getNewReleaseAlbums);
app.post("/api/admin/add-song-to-album", addSongToAlbum);
app.post("/api/admin/remove-song-from-album", removeSongFromAlbum);
app.post("/api/update-album-report", updateListenAlbumCount);
app.post("/api/toggle-like-album", toggleLikeAlbum);
app.get("/api/favourite-albums", getFavouriteAlbums);
app.post("/api/create-album", createAlbum);
app.post("/api/admin/update-album", updateAlbum);
app.post("/api/remove-album", removeAlbum);
app.get("/api/album/artist/:artistId", getAlbumFromArtist);
app.get("/api/albums/:albumId", getAlbum);

//PLAYLIST
app.post("/api/update-playlist-report", updateListenPlaylistCount);
app.post("/api/toggle-like-playlist", toggleLikePlaylist);
app.get("/api/playlists/:playlistId", getSongsFromPlaylist);
app.post("/api/playlist", createPlaylist);
app.post("/api/remove-playlist", removePlaylist);
app.get("/api/playlist", getUserPlaylist);
app.post("/api/add-song-to-playlist", addSongToPlaylist);
app.post("/api/admin/update-playlist", updatePlaylist);
app.post("/api/remove-song-from-playlist", removeSongFromPlaylist);
app.get("/api/admin/playlists", getPlaylists);

// ARTIST
app.get("/api/artists", getArtists);
app.get("/api/new-artists", getNewArtitst);
app.get("/api/song/artist/:artistId", getSongFromArtist);
app.get("/api/artist/:artistId", getCurrentArtist);
app.post("/api/admin/artist", createNewArtist);
app.post("/api/admin/update-artist", updateArtist);
app.post("/api/admin/delete-artist", deleteArtist);

// CATEGORY
app.get("/api/admin/categories", getCategories);
app.get("/api/user-categories", getUserCategories);

app.get("/api/admin/category-songs", categoriesWithTotalSongs);

app.post("/api/admin/category", createCategory);
app.post("/api/admin/update-category", updateCategory);
app.post("/api/admin/delete-category", deleteCategory);

app.get("/api/recent-list", getRecentListen);
app.get("/api/all-statistical", getAllStatistical);
app.get("/api/all-statistical-by-day", getStatisticalByDay);

// UPLOAD
app.post(
  "/api/admin/upload-song-audio",
  uploadAudios.single("audio"),
  uploadSongAudioAdmin
);
app.post(
  "/api/admin/upload-song-thumb",
  uploadImages.single("image"),
  uploadSongThumbAdmin
);
// UPLOAD_USER
app.post("/api/create-user-song", createNewUserSong);
// UPLOAD_ADMIN
app.post("/api/admin/create-new-song", createNewSong);

app.get("/api/search/:keyword", searchByKeyword);

const server = http.createServer(app);

const PORT = process.env.PORT || 8180;
server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
