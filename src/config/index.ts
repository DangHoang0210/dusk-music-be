import { getEnv } from "../utils";

export const postgresConfig = {
  host: getEnv("PG_HOST"),
  port: getEnv("PG_PORT"),
  username: getEnv("PG_USERNAME"),
  password: getEnv("PG_PW"),
  database: getEnv("PG_DB"),
};

export const jwtCongig = {
  jwtSecret: getEnv("JWT_SECRET"),
  accessTokenExpiration: "15m", // Access token expiration time (e.g., 15 minutes)
  refreshTokenExpiration: "7d",
};
