import { AppDataSource } from "../connector/postgres";
import { AlbumReport } from "../entity";

export const albumReportRepository = AppDataSource.getRepository(
  AlbumReport
).extend({});
