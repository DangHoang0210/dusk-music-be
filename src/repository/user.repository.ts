import { AppDataSource } from "../connector/postgres";
import { User } from "../entity";

export const userRepository = AppDataSource.getRepository(User).extend({
  findByUsername(username: string) {
    return this.createQueryBuilder("user")
      .where("user.username = :username", { username })
      .getOne();
  },
});
