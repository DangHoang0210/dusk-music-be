import { AppDataSource } from "../connector/postgres";
import { PlaylistReport } from "../entity";

export const playlistReportRepository = AppDataSource.getRepository(
  PlaylistReport
).extend({});
