import { AppDataSource } from "../connector/postgres";
import { SongReport } from "../entity";

export const songReportRepository = AppDataSource.getRepository(
  SongReport
).extend({});
