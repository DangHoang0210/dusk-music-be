import { AppDataSource } from "../connector/postgres";
import { Artist } from "../entity";

export const artistRepository = AppDataSource.getRepository(Artist).extend({
  async getSongFromArtist(id: number): Promise<any> {
    const qb = this.createQueryBuilder("artist")
      .where("artist.id=:id", { id })
      .leftJoinAndSelect("artist.songs", "songs") // Join table artist -> get songs
      .leftJoinAndSelect("songs.artists", "artists"); // Join songs -> list artist

    const rs: Artist | null = await qb.getOne();

    return rs;
  },
});
