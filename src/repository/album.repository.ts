import { AppDataSource } from "../connector/postgres";
import { Album } from "../entity";

export const albumRepository = AppDataSource.getRepository(Album).extend({});
