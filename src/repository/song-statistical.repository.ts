import { AppDataSource } from "../connector/postgres";
import { SongStatiscial } from "../entity";

export const songStatistialRepository = AppDataSource.getRepository(
  SongStatiscial
).extend({});
