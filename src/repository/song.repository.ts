import { AppDataSource } from "../connector/postgres";
import { Song } from "../entity";

export const songRepository = AppDataSource.getRepository(Song).extend({});
