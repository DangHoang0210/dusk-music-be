import "reflect-metadata";
import { DataSource } from "typeorm";
import { postgresConfig } from "../config";
import entities = require("../entity");

export const AppDataSource = new DataSource({
  type: "postgres",
  host: postgresConfig.host,
  port: parseInt(postgresConfig.port),
  username: postgresConfig.username,
  password: postgresConfig.password,
  database: postgresConfig.database,
  synchronize: true,
  logging: true,
  entities: Object.values(entities),
  migrations: [],
  subscribers: [],
  connectTimeoutMS: 10000,
  extra: {
    connectionLimit: 500,
  },
});
